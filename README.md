# https://docs.aws.amazon.com/cli/latest/userguide/install-linux-al2017.html

# setup
```
virtualenv -p python3 venv
venv/bin/activate
pip install awscli
export PATH=/home/ec2-user/.local/bin:$PATH
aws --version
```

# configuring aws cli
https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
- log into aws.amazon.com
- go to "My Security Credentials" under your user dropdown
- choose "Access keys"
- click "Create New Access Key"
- get access key id and secret access key from https://console.aws.amazon.com/iam/home?region=us-east-1#/security_credentials
- if you don't download or copy the secret key on create, you will not be able to see it again once you close the dialogue
aws configure

# investigating configuration
```
cat ~/.aws/credentials
cat ~/.aws/config 
aws ec2 describe-instances --profile default
export AWS_DEFAULT_PROFILE=default
aws ec2 describe-instances
```

# environment variables
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html
available vars:
- AWS_ACCESS_KEY_ID – Specifies an AWS access key associated with an IAM user or role.
- AWS_SECRET_ACCESS_KEY – Specifies the secret key associated with the access key. This is essentially the "password" for the access key.
- AWS_SESSION_TOKEN – Specifies the session token value that is required if you are using temporary security credentials. For more information, see the Output section of the assume-role command in the AWS CLI Command Reference.
- AWS_DEFAULT_REGION – Specifies the AWS Region to send the request to.
- AWS_DEFAULT_OUTPUT – Specifies the output format to use.
- AWS_DEFAULT_PROFILE – Specifies the name of the CLI profile with the credentials and options to use. This can be the name of a profile stored in a credentials or config file, or the value default to use the default profile. If you specify this environment variable, it overrides the behavior of using the profile named [default] in the configuration file.
- AWS_CA_BUNDLE – Specifies the path to a certificate bundle to use for HTTPS certificate validation.
- AWS_SHARED_CREDENTIALS_FILE – Specifies the location of the file that the AWS CLI uses to store access keys (the default is ~/.aws/credentials).
- AWS_CONFIG_FILE – Specifies the location of the file that the AWS CLI uses to store configuration profiles (the default is ~/.aws/config).

# command line flags
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-options.html
- --profile <string>
```
aws configure --profile <profilename>
```
- --region <string>
- --output <string>
- --endpoint-url <string>
- --debug
- --no-paginate
- --query <string>
- --version
- --color <string>
- --no-sign-request
- --ca-bundle <string>
- --cli-read-timeout <integer>
- --cli-connect-timeout <integer>

# when running from within an Amazon EC2 instance, you can use metadata for things like credentials
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-metadata.html

does HTTP_PROXY and NO_PROXY, etc

# woo there's a command completion script
```
echo $SHELL
cp ~/workspace/aws-cli-test/venv/bin/aws_completer ~/.local/bin/
# add this to ~/.bashrc
export PATH=/home/$USER/.local/bin:$PATH
```

```
aws s3api put-bucket-acl --bucket modle --grant-read 'uri="http://acs.amazonaws.com/groups/global/AllUsers"'
```
